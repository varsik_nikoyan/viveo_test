import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:viveo_health_test/core/mobx_state/form_store.dart';
import 'package:viveo_health_test/helper/authentication_tab_indicator.dart';

class AuthenticationPage extends StatefulWidget {
  const AuthenticationPage();

  //flutter pub get beforeAuthenticationPage({Key key}) : super(key: key);

  @override
  _AuthenticationPageState createState() => new _AuthenticationPageState();
}

class _AuthenticationPageState extends State<AuthenticationPage>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final FocusNode focusNodeEmailLogin = FocusNode();
  final FocusNode focusNodePasswordLogin = FocusNode();

  final FocusNode focusNodePassword = FocusNode();
  final FocusNode focusNodeEmail = FocusNode();
  final FocusNode focusNodeName = FocusNode();

  TextEditingController loginEmailController = new TextEditingController();
  TextEditingController loginPasswordController = new TextEditingController();

  bool _obscureTextSignup = true;

  TextEditingController signupEmailController = new TextEditingController();
  TextEditingController signupNameController = new TextEditingController();
  TextEditingController signupPasswordController = new TextEditingController();

  PageController _pageController;

  Color left = Colors.white;
  Color right = Colors.grey;
  final FormStore store = FormStore();

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
    store.setupValidations();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      body: NotificationListener<OverscrollIndicatorNotification>(
        onNotification: (overscroll) {
          overscroll.disallowGlow();
        },
        child: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height >= 775.0
                ? MediaQuery.of(context).size.height
                : 775.0,
            decoration: new BoxDecoration(
              gradient: new LinearGradient(
                  // TODO excuse me but that custom gradient background will take really long time to achieve ))
                  colors: [
                    Theme.of(context).primaryColor,
                    Theme.of(context).secondaryHeaderColor,
                  ],
                  begin: const FractionalOffset(0.0, 0.0),
                  end: const FractionalOffset(1.0, 1.0),
                  stops: [0.0, 1.0],
                  tileMode: TileMode.clamp),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 50),
                  child: new Container(
                    child: Text(
                      "Brand \n Logo",
                      style: TextStyle(color: Colors.white, fontSize: 40),
                    ),
                    width: 280.0,
                    height: 180.0,
                  ),
                ),
                Expanded(
                  // fixme I am sure I can achieve this with mobx (without even stateful widget). But I have just 4 hours to finish everything ))
                  flex: 2,
                  child: PageView(
                    controller: _pageController,
                    onPageChanged: (i) {
                      if (i == 0) {
                        setState(() {
                          right = Colors.grey;
                          left = Colors.white;
                        });
                      } else if (i == 1) {
                        setState(() {
                          right = Colors.white;
                          left = Colors.grey;
                        });
                      }
                    },
                    children: <Widget>[
                      new ConstrainedBox(
                        constraints: const BoxConstraints.expand(),
                        child: _buildSignIn(context),
                      ),
                      new ConstrainedBox(
                        constraints: const BoxConstraints.expand(),
                        child: _buildSignUp(context),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 180.0),
                  child: _buildMenuBar(context),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    focusNodePassword.dispose();
    focusNodeEmail.dispose();
    focusNodeName.dispose();

    focusNodeEmailLogin.dispose();
    focusNodePasswordLogin.dispose();

    _pageController.dispose();

    signupEmailController.dispose();
    signupNameController.dispose();
    signupPasswordController.dispose();

    loginEmailController.dispose();
    loginPasswordController.dispose();

    store.dispose();
    super.dispose();
  }

//fixme remove this magic to separate helper
  void showInSnackBar(String value) {
    FocusScope.of(context).requestFocus(new FocusNode());
    _scaffoldKey.currentState?.removeCurrentSnackBar();
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(
        value,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.white,
          fontSize: 16.0,
          fontWeight: FontWeight.bold,
        ),
      ),
      backgroundColor: Theme.of(context).accentColor,
      duration: Duration(seconds: 3),
    ));
  }

  Widget _buildMenuBar(BuildContext context) {
    return Container(
      width: 300.0,
      height: 50.0,
      decoration: BoxDecoration(
        color: Color.fromRGBO(37, 53, 66, 1),
        borderRadius: BorderRadius.all(Radius.circular(25.0)),
      ),
      child: CustomPaint(
        painter: AuthenticationTabIndicator(pageController: _pageController),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            // Pay attention on shadow ))
            Expanded(
              child: FlatButton(
                onPressed: _onSignInButtonPress,
                child: Text(
                  'Login',
                  style: TextStyle(
                    color: left,
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            //Container(height: 33.0, width: 1.0, color: Colors.white),
            Expanded(
              child: FlatButton(
                onPressed: _onSignUpButtonPress,
                child: Text(
                  'Sign up',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: right,
                    fontSize: 16.0,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildSignIn(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 23.0),
      child: Column(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width * 0.7,
            height: 190.0,
            child: Column(
              children: <Widget>[
                Observer(
                  builder: (_) => TextField(
                    onChanged: (value) => store.userName = value,
                    decoration: new InputDecoration(
                      hintText: 'Username',
                      errorText: store.error.password,
                      hintStyle: TextStyle(
                          color: Theme.of(context).accentColor, fontSize: 17.0),
                      border: new OutlineInputBorder(
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(50.0),
                        ),
                      ),
                    ),
                    focusNode: focusNodeEmailLogin,
                    controller: loginEmailController,
                    keyboardType: TextInputType.emailAddress,
                    style: TextStyle(
                        fontSize: 16.0, color: Theme.of(context).accentColor),
                  ),
                ),
                SizedBox(
                  height: 24,
                ),
                Observer(
                  builder: (_) => TextField(
                    onChanged: (value) => store.password = value,
                    decoration: new InputDecoration(
                      errorText: store.error.password,
                      hintText: 'Password',
                      hintStyle: TextStyle(
                          color: Theme.of(context).accentColor, fontSize: 17.0),
                      border: new OutlineInputBorder(
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(50.0),
                        ),
                      ),
                    ),
                    focusNode: focusNodePasswordLogin,
                    controller: loginPasswordController,
                    keyboardType: TextInputType.visiblePassword,
                    style: TextStyle(
                        fontSize: 16.0, color: Theme.of(context).accentColor),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.0),
            child: FlatButton(
                onPressed: () {
                  showInSnackBar("forgot password pressed");
                },
                child: Text(
                  'Forgot password',
                  style: TextStyle(
                    color: Theme.of(context).accentColor,
                    fontSize: 16.0,
                  ),
                )),
          ),
        ],
      ),
    );
  }

  Widget _buildSignUp(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            width: 300.0,
            height: 280.0,
            child: Column(
              children: <Widget>[
                TextField(
                  decoration: new InputDecoration(
                    hintText: 'Name',
                    hintStyle: TextStyle(
                        color: Theme.of(context).accentColor, fontSize: 17.0),
                    border: new OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(50.0),
                      ),
                    ),
                  ),
                  focusNode: focusNodeName,
                  controller: signupNameController,
                  keyboardType: TextInputType.text,
                  style: TextStyle(
                      fontSize: 16.0, color: Theme.of(context).accentColor),
                ),
                SizedBox(
                  height: 24,
                ),
                TextField(
                  decoration: new InputDecoration(
                      hintText: 'Email',
                      hintStyle: TextStyle(
                          color: Theme.of(context).accentColor, fontSize: 16.0),
                      border: new OutlineInputBorder(
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(50.0),
                        ),
                      ),
                      fillColor: Colors.white70),
                  focusNode: focusNodeEmail,
                  controller: signupEmailController,
                  keyboardType: TextInputType.emailAddress,
                  style: TextStyle(
                      fontSize: 16.0, color: Theme.of(context).accentColor),
                ),
                SizedBox(
                  height: 24,
                ),
                TextField(
                  decoration: new InputDecoration(
                      hintText: 'Password',
                      hintStyle: TextStyle(
                          color: Theme.of(context).accentColor, fontSize: 17.0),
                      border: new OutlineInputBorder(
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(50.0),
                        ),
                      ),
                      fillColor: Colors.white70),
                  focusNode: focusNodePassword,
                  controller: signupPasswordController,
                  obscureText: _obscureTextSignup,
                  style: TextStyle(
                      fontSize: 16.0, color: Theme.of(context).accentColor),
                ),
                SizedBox(
                  height: 24,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _onSignInButtonPress() {
    _pageController.animateToPage(0,
        duration: Duration(milliseconds: 500), curve: Curves.decelerate);
  }

  void _onSignUpButtonPress() {
    _pageController?.animateToPage(1,
        duration: Duration(milliseconds: 500), curve: Curves.decelerate);
  }
}
