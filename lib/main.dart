import 'package:flutter/material.dart';

import 'ui/screen/authentication_page.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: AuthenticationPage(),
        theme: ThemeData(
          brightness: Brightness.dark,
          primaryColor: Color.fromRGBO(13, 29, 48, 1),
          accentColor: Color.fromRGBO(221, 103, 94, 1),
          hintColor: Color.fromRGBO(37, 52, 67, 1),
          secondaryHeaderColor: Color.fromRGBO(30, 34, 52, 1),
        ));
  }
}
