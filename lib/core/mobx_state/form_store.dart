import 'dart:ui';

import 'package:mobx/mobx.dart';
import 'package:validators/validators.dart';
import 'package:mobx_codegen/mobx_codegen.dart';

part 'form_store.g.dart';

class FormStore = _FormStore with _$FormStore;

abstract class _FormStore with Store {
  final FormErrorState error = FormErrorState();

  @observable
  String userName = '';

  @observable
  String password = '';

  @observable
  ObservableFuture<bool> usernameCheck = ObservableFuture.value(true);

  @computed
  bool get isUserCheckPending => usernameCheck.status == FutureStatus.pending;

  @computed
  bool get canLogin => !error.hasErrors;

  List<ReactionDisposer> _disposers;

  void setupValidations() {
    _disposers = [
      reaction((_) => userName, validateEmail),
      reaction((_) => password, validatePassword)
    ];
  }

  @action
  Future validateUsername(String value) async {
    if (isNull(value) || value.isEmpty) {
      error.username = 'Cannot be blank';
      return;
    }

    try {
      usernameCheck = ObservableFuture(checkValidUsername(value));
      error.username = null;
      final isValid = await usernameCheck;
      if (!isValid) {
        error.username = 'Username cannot be 0000';
        return;
      }
    } on Object catch (_) {
      error.username = null;
    }

    error.username = null;
  }

  @action
  void validatePassword(String value) {
    error.password = isNull(value) || value.isEmpty ? 'Password can not be blank' : null;
  }

  @action
  void validateEmail(String value) {
    error.username = isEmail(value) ? null : 'Please enter valid email address';
  }

  Future<bool> checkValidUsername(String value) async {
    await Future.delayed(const Duration(seconds: 1));

    return value != '0000';
  }

  void dispose() {
    for (final d in _disposers) {
      d();
    }
  }


}

class FormErrorState = _FormErrorState with _$FormErrorState;

abstract class _FormErrorState with Store {
  @observable
  String username;

  @observable
  String password;

  @computed
  bool get hasErrors => username != null || password != null;
}
